#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>

#define BUFSIZE 100

int main(void) {
    int fd1[2], fd2[2];
    pid_t pid;

    /* Create the first pipe */
    if (pipe(fd1) == -1) {
        perror("Pipe failed");
        exit(1);
    }

    /* Create the second pipe */
    if (pipe(fd2) == -1) {
        perror("Pipe failed");
        exit(1);
    }

    pid = fork();
    if (pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    /* Parent process */
     if (pid > 0) {
        char buf[BUFSIZE];
        close(fd1[0]);   /* Close the read end of the first pipe */

        /* Write to the first pipe */
        strcpy(buf, "Hello");
        write(fd1[1], buf, strlen(buf) + 1);
        close(fd1[1]);   /* Close the write end of the first pipe */

        wait(NULL);      /* Wait for the child process to finish */

        close(fd2[1]);   /* Close the write end of the second pipe */
        read(fd2[0], buf, BUFSIZE); /* Read from the second pipe */
        printf("Parent: %s\n", buf);
        close(fd2[0]);   /* Close the read end of the second pipe */
    } 
    /* Child process */
    else {
        char buf[BUFSIZE];
        close(fd1[1]);   /* Close the write end of the first pipe */
        read(fd1[0], buf, BUFSIZE); /* Read from the first pipe */
        printf("Child: %s\n", buf);
        close(fd1[0]);   /* Close the read end of the first pipe */

        close(fd2[0]);   /* Close the read end of the second pipe */

        /* Convert to upper case */
        for (int i = 0; i < strlen(buf); i++) {
            buf[i] = toupper(buf[i]);
        }

        write(fd2[1], buf, strlen(buf) + 1);
        close(fd2[1]);   /* Close the write end of the second pipe */
    }

    return 0;
}